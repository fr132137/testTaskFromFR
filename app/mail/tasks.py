from datetime import datetime, timezone
from django.shortcuts import get_object_or_404
from django.db.models import Q
import requests
import logging

from core import settings
from core.celery import app
from mail.models import Distribution, User, Message


link = settings.LINK
token = settings.TOKEN

def create_messages(_filter:dict, distribution_id:int):
    query = Q() 

    if _filter.get('code'):
        query |= Q(code=_filter['code'])
    if _filter.get('tag'):
        query |= Q(tag=_filter['tag'])

    users = User.objects.filter(query)
    for user in users:
        message = Message.objects.create(distribution_id=distribution_id, user=user, sent_at=None)
        message.save()
    return True


@app.task
def send_message(message:dict):
    response = requests.post(url=f'{link}{message["id"]}', json=message, headers={'Authorization': f'Bearer {token}'})
    if response.status_code == 200:
        message = Message.objects.filter(id=message['id']).update( sent_at=datetime.now(timezone.utc), status=True),


@app.task
def get_message_data(distribution_id:int, end_time:datetime):
    messages = Message.objects.filter(distribution_id=distribution_id)
    for message in messages:
        data = {
            'id': message.id,
            'number': message.user.number,
            'text': message.distribution.text} 
        send_message.apply_async(args=[data], expire=end_time )

@app.task
def handle_distributions(distribution_id:int):
    distribution = get_object_or_404(Distribution, id=distribution_id)
    current_time = datetime.now(timezone.utc)
    start_time = distribution.start
    end_time = distribution.end

    if current_time > start_time and current_time < end_time:
        if create_messages(_filter=distribution.filter, distribution_id=distribution.id):
            get_message_data.apply_async(args=[distribution.id, end_time], expire=end_time)
    elif current_time < start_time and current_time < end_time:
        if create_messages(_filter=distribution.filter, distribution_id=distribution.id):
            get_message_data.apply_async(args=[distribution.id, end_time], eta=start_time, expire=end_time)
    else:
        return False