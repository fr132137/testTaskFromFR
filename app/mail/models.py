from django.db import models

class User(models.Model):
    
    number = models.DecimalField(max_digits=11, decimal_places=0, blank=True, null=True)
    code = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    time_zone = models.TextField(blank=True, null=True)
    tag = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        
class Distribution(models.Model):

    start = models.DateTimeField()
    end = models.DateTimeField()
    text = models.TextField(max_length=256, blank=True, null=True)
    filter = models.JSONField(null=True)

    class Meta:
        verbose_name = 'Distribution'
        verbose_name_plural = 'Distributions'
    


class Message(models.Model):
    
    sent_at  = models.DateTimeField(blank=True, null=True, default=None)
    status = models.BooleanField(default=False)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE, related_name='distribution')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'

