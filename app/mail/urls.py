from django.urls import path
from mail.views import UserViewSet, MessageViewSet, DistributionViewSet, DetailMessageStatisticsByDistributionView, DistributionStatistics


app_name = 'mail'
# gpd = get, put, delete
gpd_methods = {
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy',
        }

urlpatterns = [
    # YOUR PATTERNS
]

urlpatterns = [
    path('distribution/<int:pk>', view=DistributionViewSet.as_view(gpd_methods)),
    path('user/<int:pk>', view=UserViewSet.as_view(gpd_methods)),
    path('message/<int:pk>', view=MessageViewSet.as_view(gpd_methods)),

    path('distribution/', view=DistributionViewSet.as_view({'post': 'create'})),
    path('user/', view=UserViewSet.as_view({'post': 'create'})),
    path('message/', view=MessageViewSet.as_view({'post': 'create'})),
    path('distribution/<int:pk>/detail', view=DetailMessageStatisticsByDistributionView.as_view()),
    path('distribution/statistic/', view=DistributionStatistics.as_view()),
]